'use strict';

var request = require('request');
var cheerio = require('cheerio');

var fileListTool = {};

fileListTool.config = {};

// args.protocole, args.user, args.pass, args.host, args.logPath
fileListTool.formatUrl = function (args) {
    console.log(this.config);
    let defToCheck = ["protocole","user","pass","host","logPath"];
    let params = {};
    for (var i = defToCheck.length - 1; i >= 0; i--) {
        if ( args != undefined && (args[defToCheck[i]] != undefined && args[defToCheck[i]] != '' )) {
            params[defToCheck[i]] = args[defToCheck[i]];
        }
        else {
            params[defToCheck[i]] = this.config[defToCheck[i]];
        }
    };
    console.log(params);
    return params.protocole + params.user + ':' + params.pass  + '@' + params.host + params.logPath;
};

fileListTool.retrieveList = function (url, callBack) {
    this.readRemote(url, callBack);
};

fileListTool.readOne = function (url, callBack) {
    this.readRemote(url, callBack, 'file');
};

fileListTool.readRemote = function (url, callBack, mode) {
    request({
	   url: url,
	   strictSSL: false, // allow us to use our self-signed cert for testing
	   rejectUnauthorized: false,
        agent: false 
	   },
       function (error, response, body) {
            if (mode == null) {
                fileListTool.agragateFileList(error, response, body, callBack);
            }
            else {
                fileListTool.readFile(error, response, body, callBack);   
            }
    });
};

fileListTool.agragateFileList = function (error, response, body, callBack) {
    if (!error && response != undefined && response.statusCode == 200) {
        var $ = cheerio.load(body);
        var fileLogList = [];
        $('table tr').each(function(i, element){
            if ( i != 0 && i < 5 ) {
                var formatedLine = {filename: '', size: '', lastmodified: ''};
                formatedLine.filename = $(this).find('td:nth-child(1) tt').html();
                formatedLine.size = $(this).find('td:nth-child(2) tt').html() || 'folder';
                formatedLine.lastmodified = $(this).find('td:nth-child(3) tt').html();
                fileLogList.push(formatedLine);
            }
        });
        callBack(fileLogList);
    } else {
        console.log('some error occured :');
        console.log(error);
    }
};

fileListTool.readFile = function (error, response, body, callBack) {
    if (!error && response != undefined && response.statusCode == 200) {
        if (body == undefined || body == '') {
            body = 'no-line to show';
        }
        let arrayOfLines = body.match(/[^\r\n]+/g);
        callBack({lineNumber: arrayOfLines.length, lines: arrayOfLines});
    } else {
        console.log('some error occured :');
        console.log(error);
    }
};

module.exports = fileListTool;
