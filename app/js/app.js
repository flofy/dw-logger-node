'use strict';

var fileListTool = require('./asrc/fileList');

var low = require('lowdb');
var config = low('config.json');

var App = angular.module('App', [
  'ngRoute',
  'appControllers'
]);

App.config(['$routeProvider',

function($routeProvider) {
  $routeProvider.
    when('/', {
      templateUrl: 'views/main.html',
      controller: 'IndexCtrl'
    }).  
    otherwise({
      redirectTo: '/'
    });
}]);
