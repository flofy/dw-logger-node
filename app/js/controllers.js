'use strict';

var appControllers = angular.module('appControllers', []);

appControllers

.controller('IndexCtrl', ['$scope', '$routeParams',
  function($scope, $routeParams){
      let init = [];
      
      let tabs = [{
              title: 'Liste des servers logable',
              url: 'mngLog',
              tpl: 'views/logTab/main.html'
          }, {
              title: 'List des fichiers de Log',
              url: 'logs',
              tpl: 'views/logTab/list.html'
          }, {
              title: 'OneFile',
              url: 'fileLog',
              tpl: 'views/logTab/tail.html'
      }];
      $scope.tabs = tabs;
      $scope.currentTab = tabs[1].tpl;
      
      fileListTool.config = config('common').cloneDeep();

      $scope.hideCredential = true;

  
      $scope.onClickTab = function (tab) {
          if (tab == 'logs') {
            fileListTool.formatUrl();
          }
          $scope.currentTab = tab.tpl;
      }
      
      $scope.isActiveTab = function(tabTpl) {
          return tabTpl == $scope.currentTab;
      }

      $scope.search = {filename: '',size: '!folder',lastmodified: '', $:''};

      $scope.logfiles = init;

      
      $scope.onSelectSite = function(siteId) {
        let siteConfig = config('sites').find({id: siteId});
        let url = fileListTool.formatUrl(siteConfig);
        if (url != undefined) {
          fileListTool.retrieveList(url, function (data){ 
            $scope.$apply(function(){
              $scope.logfiles = data;
            });
          });
        }
      }

      $scope.viewFile = function(filename, siteId){
        let siteConfig = config('sites').find({id: siteId});
        let url = fileListTool.formatUrl(siteConfig);
        fileListTool.readOne(url + filename, function (data){
          $scope.$apply(function(){
            $scope.file = {
                filename: filename,
                fileContent: data
            };
            $("#modalView").modal();
          });
        });
      };

      $scope.sites = config('sites').cloneDeep();
      
      $scope.siteSelect = {
        singleSelect: null,
        options: $scope.sites
      };

      $scope.site = {
        "name": "name your site",
        "host": "dev0x-store-lacoste.demandware.net/",
        "user": "user",
        "pass": "pass"
      };
      
      $scope.removeSite = function(site){
        var query = {name: site.name, host: site.host};
        config('sites').remove(query);
        $scope.sites = config('sites').cloneDeep();
      };

      $scope.viewSite = function(site){
        $scope.site = site;
        $("#modalView").modal();
      };

      $scope.editSite = function(site){
          var t = $scope.site;
          if ( t == undefined ) {
              // we are on new Mode
              t = site;
          }
          // i have to put some validator to avoid empty data
          var data = {name: t.name, host: t.host};
          if (t.user != '' || t.user != undefined) {
            data.user = t.user;
          }
          if (t.pass != '' || t.pass != undefined) {
            data.pass = t.pass;
          }
          if (site.id != undefined) {
              data.id = site.id;
              config('sites').chain().find({id: site.id}).assign(data).value();
          } else {
              data.id = data.name.replace('[^0-9a-z]', '') + new Date();
              config('sites').push(data);
          }
          $scope.sites = config('sites').cloneDeep();
          $("#modalView").modal('toggle');
      }
  }
]);
